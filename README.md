# Supported tags and respective `Dockerfile` links

  - [`10.14.2.0`, `10.14.2`, `10.14`, `latest` (10.14/Dockerfile)](https://bitbucket.org/kazssym/derby-docker/src/master/10.14/Dockerfile)
  - [`10.13.1.1`, `10.13.1`, `10.13` (10.13/Dockerfile)](https://bitbucket.org/kazssym/derby-docker/src/master/10.13/Dockerfile)
  - [`10.12.1.1`, `10.12.1`, `10.12` (10.12/Dockerfile)](https://bitbucket.org/kazssym/derby-docker/src/master/10.12/Dockerfile)

**Note** builds are no longer automated.

# Description

[Apache Derby][] is an [open source][] relational database management system implemented entirely in Java.
This repository contains unofficial images for Derby Network Server.

[Apache Derby]: https://db.apache.org/derby/
[open source]: https://opensource.org/osd "The Open Source Definition"
